import React, { lazy, Suspense } from "react";
const Button = lazy(() => import("./pages/components/button"));
export { Button };
